<?php
//muito útil para DEBUG (NUNCA USAR EM PRODUÇÃO)
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//FIM muito útil para DEBUG (NUNCA USAR EM PRODUÇÃO)

$despesas[0] = 345.55;
$despesas[1] = 135.00;
$despesas[2] = 600.00;
$despesas[3] = 900.00;
$despesas[4] = 400.00;

for ($i = 0; $i < 5; $i++) {

    echo $despesas[$i] . "<br>";
}

unset($despesas);//Apagua/destrói o vetor

$despesas['mercado'] = 345.55;
$despesas['estacionamento'] = 1935.00;
$despesas['alimentacao'] = 600.00;
$despesas['bar'] = 900.00;
$despesas['educacao'] = 4000.00;

echo "<br>Despesas<br>";

foreach ( $despesas as $nome /*índice*/ => $gasto /*valor*/ ) {

    echo "$nome: R$ " . number_format($gasto, 2, ',', '.') . "<br>";
}



